<?php

namespace Tests\Unit\FormRequests;

use \PHPUnit\Framework\TestCase;
use Vapest\Restful\FormRequests\RestApiFormRequest;

/**
 * Class RestApiFormRequestTest
 *
 * @package Tests\Unit\FormRequests
 */
class RestApiFormRequestTest extends TestCase
{
    /**
     * @test
     */
    public function classExists(): void
    {
        $this->assertTrue(
            class_exists(RestApiFormRequest::class),
            'Failed to verify that class exists'
        );
    }

    /**
     * @test
     */
    public function restApiFormRequestWorks()
    {
        $req = new class extends RestApiFormRequest
        {
            public function authorize(): bool
            {
                return true;
            }

            public function rules(): array
            {
                return [
                    'rule' => [
                        'one' => 123
                    ]
                ];
            }

            public function setShouldCheckXwww(bool $state): void
            {
                $this->shouldCheckXwwwForm = $state;
            }

            /**
             * @param string $param
             *
             * @return mixed
             */
            public function getParam(string $param)
            {
                return $this->{$param};
            }
        };

        $req->setShouldCheckXwww(false);
        $shouldCheckXwwwForm = $req->getParam('shouldCheckXwwwForm');
        $this->assertFalse($shouldCheckXwwwForm);

        $req->setShouldCheckXwww(true);
        $shouldCheckXwwwForm = $req->getParam('shouldCheckXwwwForm');
        $this->assertTrue($shouldCheckXwwwForm);

        $rules = $req->rules();
        $this->assertEquals(
            [
                'rule' => [
                    'one' => 123
                ]
            ],
            $rules
        );
    }
}
