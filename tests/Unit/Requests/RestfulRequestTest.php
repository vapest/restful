<?php

namespace Tests\Unit\Requests;

use \PHPUnit\Framework\TestCase;
use Vapest\Restful\Requests\RestfulRequest;

/**
 * Class RestfulRequestTest
 *
 * @package Tests\Unit\Requests
 */
class RestfulRequestTest extends TestCase
{
    /**
     * @var RestfulRequest
     */
    private $restfulRequest;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->restfulRequest = new RestfulRequest();
    }

    /**
     * @test
     */
    public function classExists(): void
    {
        $this->assertTrue(
            class_exists(RestfulRequest::class),
            'Failed to verify that class exists'
        );
    }

    /**
     * @test
     */
    public function requestWantsJson(): void
    {
        $this->assertTrue(
            $this->restfulRequest->wantsJson(),
            'Failed to verify request wants json'
        );
    }

    /**
     * @test
     */
    public function requestExpectsJson(): void
    {
        $this->assertTrue(
            $this->restfulRequest->expectsJson(),
            'Failed to verify request expects json'
        );
    }
}
