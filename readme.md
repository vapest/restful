# Package Description

This package provides Request class replacement.

As the side features:

* RestApiFormRequest.php - class to be extended by your Form Request classes

## Configuration

### Base request of the application

in your public/index.php replace

```
$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);
```

with

```
$response = $kernel->handle(
    $request = \Vapest\Restful\Requests\RestfulRequest::capture()
);
```

### Form request of the application

Use RestApiFormRequest.php as main parent for your Form Requests.
