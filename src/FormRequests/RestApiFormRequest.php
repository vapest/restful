<?php

namespace Vapest\Restful\FormRequests;

use Vapest\Restful\Exceptions\RestApiFormRequestException;
use \Illuminate\Contracts\Validation\Validator;
use \Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Auth\Access\AuthorizationException;
use \Illuminate\Http\Response;

/**
 * Class RestApiFormRequest.
 *
 * @package Vapest\Restful\FormRequests
 */
abstract class RestApiFormRequest extends FormRequest
{
    protected const METHODS_USES_X_WWW_FORM = ['PUT', 'PATCH'];

    /**
     * @var bool
     */
    protected $shouldCheckXwwwForm = true;

    /**
     * RestApiFormRequest constructor.
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null  $content
     *
     * @throws RestApiFormRequestException
     */
    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        if ($this->shouldCheckXwwwForm() && in_array($this->getMethod(), self::METHODS_USES_X_WWW_FORM)) {
            $this->preprocessUpdateRequest();
        }
    }

    /**
     * Get condition.
     *
     * @return bool
     */
    protected function shouldCheckXwwwForm(): bool
    {
        return $this->shouldCheckXwwwForm;
    }

    /**
     * Update requests forced to use X-www-form-urlencoded
     *
     * @throws RestApiFormRequestException
     */
    protected function preprocessUpdateRequest(): void
    {
        if (null !== $this->getQueryString()) {
            throw new RestApiFormRequestException(
                'Your request cannot contain query string. It must use x-www-form-urlencoded.',
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * Overrides default behavior of authorization failed action.
     *
     * @throws AuthorizationException
     */
    protected function failedAuthorization(): void
    {
        throw new AuthorizationException('Unhandled Authorization failed method in ' . static::class);
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()
                     ->after(function (Validator $validator) {
                         $this->after($validator);
                     });
    }

    /**
     * Allows to register "After" hook.
     *
     * @param Validator $validator
     */
    protected function after(Validator $validator): void
    {
        // override it in your class
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize(): bool;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules(): array;
}
