<?php

namespace Vapest\Restful\Requests;

use \Illuminate\Http\Request;

/**
 * Class RestfulRequest.
 *
 * @package Vapest\Restful\Requests
 */
class RestfulRequest extends Request
{
    /**
     * Request expects JSON input.
     *
     * @return bool
     */
    public function expectsJson(): bool
    {
        return true;
    }

    /**
     * Request will return JSON.
     *
     * @return bool
     */
    public function wantsJson(): bool
    {
        return true;
    }
}
