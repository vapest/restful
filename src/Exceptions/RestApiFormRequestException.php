<?php

namespace Vapest\Restful\Exceptions;

use \Exception;

/**
 * Class RestApiFormRequestException
 *
 * @package Vapest\Restful\Exceptions
 */
class RestApiFormRequestException extends Exception
{
    //
}
